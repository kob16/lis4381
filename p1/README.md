> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381 Mobile Web App Development

## Keagan Bogart

### P1 Requirements:

*Three Parts:*

1. Create business card app with contact info and interests
2. Create launcher icon and display it in both activities 
3. Add background color to both activities
4. Add border around image and button
5. Add text shadow to button

#### README.md file should include the following items:

* Screenshot of running applications first user interface
* Screenshot of running applications second user interface
* git commands w/short descriptions
* Bitbucket repo links: a) this assignment and b) the completed tutorials above

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init- creates a new Git repository
2. git status- displays the state of the working directory and the staging area
3. git add- adds a change in the working directory to the staging area
4. git commit- saves changes to the local repository
5. git push- uploads local repository content to remote repository
6. git pull- downloads content from remote repository and updates local repository to match
7. git clone- makes a copy of an existing repository

#### Assignment Screenshots:

| *Screenshot of first user interface*               | *Screenshot of second user interface*          |
|:--------------------------------------------------:|:----------------------------------------------:|
| ![First] (img/firstscreen.png)                     | ![Second] (img/secondscreen.png)               |


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")
