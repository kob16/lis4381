> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381 Mobile Web App Development

## Keagan Bogart

### Assignment # 4 Requirements:

*Three Parts:*

1. Clone A4 starter files
2. JQuery validation and regualr expressions
3. Create favicon
4. Customzie online portfolio

#### README.md file should include the following items:

* Screenshot of main portal
* Screenshot of failed validation
* Screenshot of passed validation
* git commands w/short descriptions
* Bitbucket repo links: a) this assignment and b) the completed tutorials above

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init- creates a new Git repository
2. git status- displays the state of the working directory and the staging area
3. git add- adds a change in the working directory to the staging area
4. git commit- saves changes to the local repository
5. git push- uploads local repository content to remote repository
6. git pull- downloads content from remote repository and updates local repository to match
7. git clone- makes a copy of an existing repository

#### Assignment Screenshots:

| *Screenshot of main portal*                        | *Screenshot of failed validation*              |
|:--------------------------------------------------:|:----------------------------------------------:|
| ![Home] (img/home.png)                       | ![Failed] (img/failed.png)               |

| *Screenshot of passed validation*               |
|:--------------------------------------------------:|
| ![Passed] (img/passed.png)                     | 




#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")