> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381 Mobile Web App Development

## Keagan Bogart

### Assignment # 2 Requirements:

*Two Parts:*

1. Create a mobile recipe app using android studio
2. Chapter Questions (Chs 3-4)

#### README.md file should include the following items:

* Screenshot of first user interface
* Screenshot of second user interface
* Assignment requirements
* git commands w/short descriptions
* Bitbucket repo link to this assignment

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init- creates a new Git repository
2. git status- displays the state of the working directory and the staging area
3. git add- adds a change in the working directory to the staging area
4. git commit- saves changes to the local repository
5. git push- uploads local repository content to remote repository
6. git pull- downloads content from remote repository and updates local repository to match
7. git clone- makes a copy of an existing repository

#### Assignment Screenshots:

*Screenshot of first user interface running:

![First User Interface Screenshot](img/first.png)

*Screenshot of second user interface:

![Second User Interface Screenshot](img/second.png)

*Link to bitbucket repo: https://kob16@bitbucket.org/kob16/lis4381.git
