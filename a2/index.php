<!DOCTYPE html>
<html lang="en">
<head>
<!--"Time-stamp: <Sun, 05-27-18, 19:34:59 Eastern Daylight Time>"//-->
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
	<meta name="author" content="Your Name Here!">
	<link rel="icon" href="k.png">

	<title>LIS 4381 - Project 1</title>
		<?php include_once("../css/include_css.php"); ?>
</head>
<body>

	<?php include_once("../global/nav.php"); ?>

	<div class="container">
		<div class="starter-template">
					
					<div class="page-header">
						<?php include_once("global/header.php"); ?>	
					</div>

					<!-- Carousel styles -->
<style type="text/css">
h2
{
	margin: 0;     
	color: #666;
	padding-top: 50px;
	font-size: 52px;
	font-family: "trebuchet ms", sans-serif;    
}
.item
{
	background: #333;    
	text-align: center;
	height: 300px !important;
}
.carousel
{
  margin: 20px 0px 20px 0px;
  
}
.bs-example
{
  margin: 20px;
}
</style>

</head>
<body>

                <?php include_once("global/nav_global.php"); ?>
               
                <div class="container">
                        <div class="starter-template">
                                <div class="page-header">
                                        <?php include_once("global/header.php"); ?>    
                                </div>

                                <!-- Start Bootstrap Carousel  -->
                                <div class="bs-example">
                                        <div
                                                id="myCarousel"
                                                                class="carousel"
                                                                data-interval="1000"
                                                                data-pause="hover"
                                                                data-wrap="true"
                                                                data-keyboard="true"                   
                                                                data-ride="carousel">
                                               
                                 <!-- Carousel indicators -->
                                                <ol class="carousel-indicators">
                                                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                                                        <li data-target="#myCarousel" data-slide-to="1"></li>
                                                        <li data-target="#myCarousel" data-slide-to="2"></li>
                                                </ol>  
                                                <!-- Carousel items -->
                                                <div class="carousel-inner">

                                                        <div class="active item" style="background: url(img/first.png) no-repeat left center; background-size: cover;">
                                                                <div class="container">
                                                               <!-- <img src="img/slide1-11.jpg" alt="Slide 1"> -->
                                                                        <div class="carousel-caption">
                                                                                <h3>First User Interface</h3>
                                                                                
                                                                        </div>
                                                                </div>
                                                        </div>                                 

                                                        <div class="item" style="background: url(img/second.png) no-repeat left center; background-size: cover;">
                                                        <!-- <img src="img/slide2.jpg" alt="Slide 2"> -->
                                                                <div class="carousel-caption">
                                                                        <h3>Second User Interface</h3>
                                                                        
                                                                                                                    
                                                                </div>
                                                        </div>

                                                       
                                                </div>
                                                <!-- Carousel nav -->
                                                <a class="carousel-control left" href="#myCarousel" data-slide="prev">
                                                        <span class="glyphicon glyphicon-chevron-left"></span>
                                                </a>
                                                <a class="carousel-control right" href="#myCarousel" data-slide="next">
                                                        <span class="glyphicon glyphicon-chevron-right"></span>
                                                </a>
                                        </div>
                                </div>
                                <!-- End Bootstrap Carousel  -->

			<?php include_once "global/footer.php"; ?>
			
		</div> <!-- end starter-template -->
 </div> <!-- end container -->

	
	<!-- Bootstrap JavaScript
	================================================== -->
	<!-- Placed at end of document so pages load faster -->
			<?php include_once("../js/include_js.php"); ?>

<script type="text/javascript">
 //See Regular Expressions: http://www.qcitr.com/usefullinks.htm#lesson7
 $(document).ready(function() {

	$('#defaultForm').formValidation({
			message: 'This value is not valid',
			icon: {
					valid: 'fa fa-check',
					invalid: 'fa fa-times',
					validating: 'fa fa-refresh'
			},
			fields: {
					name: {
							validators: {
									notEmpty: {
									 message: 'Name required'
									},
									stringLength: {
											min: 1,
											max: 30,
									 message: 'Name no more than 30 characters'
									},
									regexp: {
										//alphanumeric, hyphens, underscores, and spaces
										//regexp: /^[a-zA-Z0-9\-_\s]+$/,										
										//similar to: (though, \w supports other Unicode characters)
											regexp: /^[\w\-\s]+$/,
										message: 'Name can only contain letters, numbers, hyphens, and underscore'
									},									
							},
					},

					street: {
							validators: {
									notEmpty: {
											message: 'Street required'
									},
									stringLength: {
											min: 1,
											max: 30,
											message: 'Street no more than 30 characters'
									},
									regexp: {
										//street: only letters, numbers, commas, hyphens, space character, and periods
										regexp: /^[a-zA-Z0-9,\-\s\.]+$/,		
									message: 'Street can only contain letters, numbers, commas, hyphens, or periods'
									},									
							},
					},
					
			}
	});
});
</script>

</body>
</html>
