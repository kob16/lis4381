# LIS4381 - Mobile Web Application Development

## Keagan Bogart

### LIS4381 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Distributed Version Control with Git and Bitbucket
    - Development Installations
    - Chapter Questions (Chs 1,2)

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Created Bruschetta Recipe app
    - Created first user interface with button
    - Created second user interface (Recipe & directions)
    - Chapter Questions (Chs 3,4)

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Create event ticket app
    - Create launcher icon image and display it in both activities (screens)
    - Create ERD's with the ability to forward engineer
    
4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Clone A4 starter files
    - JQuery validation and regualr expressions
    - Create favicon
    - Customzie online portfolio
    
5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Use A4 cloned files
    - Edit meta-tags
    - Change title, headers, and nav links
    - Server side validation

6. [P1 README.md](p1/README.md "My P1 README.md file")
    - Create business card app with contact info and interests
    - Create launcher icon and display it in both activities 
    - Add background color to both activities
    - Add border around image and button
    - Add text shadow to button

7. [P2 README.md](p2/README.md "My P2 README.md file")
    -  Requires A4 cloned files
    - Server-side validation
    - Edit & delete button
    - RSS feed