> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381 Mobile Web App Development

## Keagan Bogart

### P2 Requirements:


1. Requires A4 cloned files
2. Server-side validation
3. Edit & delete button
4. RSS feed

#### README.md file should include the following items:

* Screenshot of index.php
* Screenshot of edit_petstore.php
* Screenshot of edit_petstore_process.php
* Screenshot of Carousel
* Screenshot of RSS feed
* git commands w/short descriptions
* Bitbucket repo links: a) this assignment and b) the completed tutorials above

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init- creates a new Git repository
2. git status- displays the state of the working directory and the staging area
3. git add- adds a change in the working directory to the staging area
4. git commit- saves changes to the local repository
5. git push- uploads local repository content to remote repository
6. git pull- downloads content from remote repository and updates local repository to match
7. git clone- makes a copy of an existing repository

#### Assignment Screenshots:

| *Screenshot 1*                                     | *Screenshot 2*                                 |
|:--------------------------------------------------:|:----------------------------------------------:|
| ![First] (img/screen1.png)                         | ![Second] (img/screen2.png)                    |
| *Screenshot 3*                                     | *Screenshot 4*                                 |
|:--------------------------------------------------:|:----------------------------------------------:|
| ![Third] (img/screen3.png)                         | ![Fourth] (img/screen4.png)                    |
| *Screenshot 5*                                     |
|:--------------------------------------------------:|
| ![Fifth] (img/screen5.png)                         | 

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")
